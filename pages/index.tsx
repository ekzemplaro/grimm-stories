import Link from 'next/link'
import Layout from '../components/layout'
import styles from '../styles/Home.module.css'
 
export default function Home() {
    return (
        <Layout title="グリムの昔話" description="グリムの昔話概要" >
            <p>グリムの昔話を紹介するページです。</p>
        </Layout>
    )
}
