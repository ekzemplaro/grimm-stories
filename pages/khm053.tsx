import Link from 'next/link'
import Layout from '../components/layout'
import styles from '../styles/Home.module.css'
 
export default function About() {
    return (
<Layout title="白雪姫" description="白雪姫 概要" >
            <p>白雪姫 概要</p>
<p>昔、真冬に、雪が羽のようにチラチラと空から降っているとき、窓のところでお后が縫物をしていました。窓枠は黒檀でできており、縫物をして窓から雪を見ている間に、お后は針で指を刺してしまい、３滴の血が雪の上に落ちました。その赤は白い雪の上できれいに見え、お后は「雪のように白く、血のように赤く、窓枠の木のように黒い子供が欲しいわ…」と思いました。</p>
        </Layout>
    )
}

